<?php
namespace PHPSTORM_META {

    override(\Miniframe\Annotation\Model\Property::getAnnotation(), map([
        \Factory::classConstant => '@Class',
        // If a default is possible, use that one instead, but this one is common;
        'var' => \Miniframe\Annotation\Annotation\BaseAnnotation::class
    ]));

    override(\Miniframe\Annotation\Model\ClassInfo::getAnnotation(), map([
        \Factory::classConstant => '@Class',
        // If a default is possible, use that one instead, but this one is common;
        'var' => \Miniframe\Annotation\Annotation\BaseAnnotation::class
    ]));
//
//    // Suggests constants as fourth argument in StreamResponse::__construct()
//    expectedArguments(
//        \Miniframe\Response\StreamResponse::__construct(),
//        3,
//        \Miniframe\Response\StreamResponse::DISPOSITION_ATTACHMENT,
//        \Miniframe\Response\StreamResponse::DISPOSITION_INLINE
//    );
}
