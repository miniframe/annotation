# Miniframe Annotation Middleware

This package helps working with annotations within the [Miniframe PHP Framework](https://miniframe.dev/).

[![build](https://miniframe.dev/build/badge/annotation)](https://bitbucket.org/miniframe/annotation/addon/pipelines/home)
[![code coverage](https://miniframe.dev/codecoverage/badge/annotation)](https://miniframe.dev/codecoverage/annotation)

## How to use

At this point, this package isn't stand-alone, but a requirement for other packages.

## For Windows Developers

In the `bin` folder, a few batch files exist, to make development easier.

If you install [Docker Desktop for Windows](https://www.docker.com/products/docker-desktop),
you can use [bin\composer.bat](bin/composer.bat), [bin\phpcs.bat](bin/phpcs.bat), [bin\phpunit.bat](bin/phpunit.bat), [bin\phpstan.bat](bin/phpstan.bat) and [bin\security-checker.bat](bin\security-checker.bat) as shortcuts for Composer, CodeSniffer, PHPUnit, PHPStan and the Security Checker, without the need of installing PHP and other dependencies on your machine.

The same Docker container and tools are used in [Bitbucket Pipelines](https://bitbucket.org/miniframe/annotation/addon/pipelines/home) to automatically test this project.
