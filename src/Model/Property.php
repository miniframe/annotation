<?php

namespace Miniframe\Annotation\Model;

class Property
{
    /**
     * Name of the property
     *
     * @var string
     */
    protected $name;
    /**
     * Access level
     *
     * @var string
     */
    protected $accessLevel;
    /**
     * List of all annotations
     *
     * @var object[]
     */
    protected $annotations;

    /**
     * Property data model
     *
     * @param string   $name        Name of the property.
     * @param string   $accessLevel Access level.
     * @param object[] $annotations List of annotations.
     */
    public function __construct(string $name, string $accessLevel, array $annotations)
    {
        $this->name = $name;
        $this->accessLevel = $accessLevel;
        $this->annotations = $annotations;
    }

    /**
     * Returns the name of the property
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Returns the access level
     *
     * @return string
     */
    public function getAccessLevel(): string
    {
        return $this->accessLevel;
    }

    /**
     * Returns a list of all annotations
     *
     * @return object[]
     */
    public function getAnnotations(): array
    {
        return $this->annotations;
    }

    /**
     * Returns a single annotation
     *
     * @param string $name Name of the annotation.
     *
     * @return object|null
     */
    public function getAnnotation(string $name)
    {
        return $this->annotations[$name] ?? null;
    }
}
