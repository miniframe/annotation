<?php

namespace Miniframe\Annotation\Model;

class ClassInfo
{
    /**
     * Classname including namespace
     *
     * @var string
     */
    protected $fullyQualifiedClassName;
    /**
     * List of annotations
     *
     * @var object[]
     */
    protected $annotations;
    /**
     * List of properties
     *
     * @var Property[]
     */
    protected $properties;

    /**
     * Class data model
     *
     * @param string     $fullyQualifiedClassName Classname including the namespace.
     * @param object[]   $annotations             List of annotations.
     * @param Property[] $properties              List of properties.
     */
    public function __construct(string $fullyQualifiedClassName, array $annotations, array $properties)
    {
        $this->fullyQualifiedClassName = $fullyQualifiedClassName;
        $this->annotations = $annotations;
        $this->properties = $properties;
    }

    /**
     * Returns the fully qualified classname
     *
     * @return string
     */
    public function getFullyQualifiedClassName(): string
    {
        return $this->fullyQualifiedClassName;
    }

    /**
     * Returns the short classname (without namespace)
     *
     * @return string
     */
    public function getShortClassName(): string
    {
        return array_reverse(explode('\\', $this->fullyQualifiedClassName))[0];
    }

    /**
     * Returns a list of all annotations
     *
     * @return object[]
     */
    public function getAnnotations(): array
    {
        return $this->annotations;
    }

    /**
     * Returns a single annotation
     *
     * @param string $name Name of the annotation.
     *
     * @return object|null
     */
    public function getAnnotation(string $name)
    {
        return $this->annotations[$name] ?? null;
    }

    /**
     * Returns a list of all properties
     *
     * @return Property[]
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * Returns a single property
     *
     * @param string $propertyName Name of the property.
     *
     * @return Property|null
     */
    public function getProperty(string $propertyName): ?Property
    {
        return $this->properties[$propertyName] ?? null;
    }
}
