<?php

namespace Miniframe\Annotation\Annotation;

/**
 * Base annotation
 *
 * @Annotation
 */
class BaseAnnotation
{
    /**
     * Name of the base annotation (var, author, etc.)
     *
     * @var string
     */
    public $name;

    /**
     * Value of the annotation
     *
     * @var string
     */
    public $value;

    /***
     * Initiates a new base annotation
     *
     * @param string $name  Name of the base annotation (var, author, etc.).
     * @param string $value Value of the annotation.
     */
    public function __construct(string $name, string $value)
    {
        $this->name = $name;
        $this->value = $value;
    }
}
