<?php

namespace Miniframe\Annotation\Service;

use Miniframe\Annotation\Model\Property;
use Miniframe\Annotation\Model\ClassInfo;
use Miniframe\Annotation\Annotation\BaseAnnotation;

class AnnotationReader
{
    /**
     * Returns metadata about a class, including annotated properties
     *
     * @param \ReflectionClass $reflectionClass A reflection of the class (new \ReflectionClass($class)).
     *
     * @return ClassInfo
     */
    public function getClass(\ReflectionClass $reflectionClass): ClassInfo
    {
        return new ClassInfo(
            $reflectionClass->getName(),
            $this->parseDocBlock($reflectionClass, $reflectionClass->getDocComment()),
            $this->getProperties($reflectionClass)
        );
    }

    /**
     * Returns an array with all properties in the reflected class, including parsed doc blocks.
     *
     * @param \ReflectionClass $rc The reflected class.
     *
     * @return Property[]
     */
    private function getProperties(\ReflectionClass $rc): array
    {
        $properties = $rc->getProperties();

        $return = array();
        foreach ($properties as $property) {
            if ($property->isStatic()) {
                continue;
            }

            if ($property->isPrivate()) {
                $access = 'private';
            } elseif ($property->isProtected()) {
                $access = 'protected';
            } elseif ($property->isPublic()) {
                $access = 'public';
            } else {
                throw new \RuntimeException('The property is not defined properly');
            }

            $return[$property->getName()] = new Property(
                $property->getName(),
                $access,
                $this->parseDocBlock($rc, $property->getDocComment())
            );
        }

        return $return;
    }

    /**
     * Fetches an array of use statements in a class file and returns a string with the alias as key and FQCN as value
     *
     * @param \ReflectionClass $rc     The reflection class.
     * @param string           $prefix Optionally a prefix that's prepended to the key and value.
     *
     * @return string[]
     */
    private function getUseStatements(\ReflectionClass $rc, string $prefix = ''): array
    {
        $filename = $rc->getFileName();

        $data = file_get_contents($filename);
        preg_match_all(
            "/(^|;)[\s]*use[\s]+(?P<class>.*?)(|[\s]+as[\s]+(?<alias>.*?))[\s]*;/mu",
            $data,
            $matches,
            PREG_SET_ORDER
        );
        $return = array();
        foreach ($matches as $match) {
            $short = array_reverse(explode('\\', $match['class']))[0];
            $alias = $prefix . ($match['alias'] ?? $short);
            $return[$alias] = $prefix . $match['class'];
        }
        return $return;
    }

    /**
     * Parses a docblock string, returning annotation classes
     *
     * @param \ReflectionClass $rc       Reflection class.
     * @param string           $docblock The docblock string.
     *
     * @return object[]
     */
    private function parseDocBlock(\ReflectionClass $rc, string $docblock): array
    {
        if (
            substr($docblock, 0, 3) !== '/**'
            || substr($docblock, -2) !== '*/'
        ) {
            return [];
        }

        // Replace @Annotation to @Full\Namespace\Annotation
        $useStatements = $this->getUseStatements($rc, '@');
        $docblock = str_replace(
            array_keys($useStatements),
            array_values($useStatements),
            $docblock
        );

        // Remove start en end of docblock
        $docblock = substr($docblock, 3, -2);

        // Remove the asterisk signs at the beginning of each line
        $docblock = preg_replace('/^[ \t]*\*[ \t]*/m', '', $docblock);

        $return = array();
        $annotations = preg_split('/^@/m', $docblock);
        foreach ($annotations as $iterator => $annotation) {
            // First part is always title + description (which both may be empty)
            if ($iterator === 0) {
                $split = explode("\n", trim($annotation), 2);
                $return['_title'] = trim($split[0]) ?? null;
                $return['_description'] = trim($split[1] ?? '') ?? null;
                continue;
            }

            // We've got an annotation, let's split its name and its properties
            $split = preg_split('/[\s\(]+/', $annotation, 2);
            $annotationClassname = trim($split[0]);
            $annotationProperties = substr($annotation, strlen($annotationClassname));

            // Auto guess namespace
            if (!class_exists($annotationClassname) && class_exists('Miniframe\\Annotation\\' . $annotationClassname)) {
                $annotationClassname = 'Miniframe\\Annotation\\' . $annotationClassname;
            }

            // Isn't it an annotation class? Then treat it as a generic string
            if (!class_exists($annotationClassname)) {
                $return[strtolower($annotationClassname)] = new BaseAnnotation(
                    $annotationClassname,
                    trim($annotationProperties)
                );
                continue;
            }

            // Is it an annotation class? Initiate the class.
            $return[$annotationClassname] = $this->injectProperties(
                $annotationClassname,
                $this->parseProperties($annotationProperties)
            );
        }
        return $return;
    }

    /**
     * Converts an annotation class name and an array of properties to an annotation object
     *
     * @param string $annotationClassname  Name of the annotation class.
     * @param array  $annotationProperties Array of properties.
     *
     * @return object
     */
    private function injectProperties(string $annotationClassname, array $annotationProperties): object
    {
        $annotationInfo = $this->getClass(new \ReflectionClass($annotationClassname));

        try {
            $annotationClass = new $annotationClassname();
        } catch (\Throwable $throwable) {
            // If loading the annotation class fails, fall back to a base annotation
            return new BaseAnnotation($annotationClassname, '');
        }
        foreach ($annotationProperties as $property => $value) {
            $types = strtolower($annotationInfo->getProperty($property)->getAnnotation('var')->value ?? 'string');
            $type = explode('|', $types, 2)[0];
            switch ($type) {
                case 'int':
                case 'integer':
                    $annotationClass->$property = intval($value, 10);
                    break;
                case 'float':
                    $annotationClass->$property = floatval($value);
                    break;
                case 'bool':
                case 'boolean':
                    $annotationClass->$property =
                        ($value === '' || $value === '1' || $value === 'true' || $value === true);
                    break;
                default:
                    $annotationClass->$property = $value;
            }
        }
        return $annotationClass;
    }

    /**
     * Returns an array with key 'property' and it's value
     *
     * @param string $properties The data as string.
     *
     * @return array
     */
    private function parseProperties(string $properties): array
    {
        preg_match('/^[\s]*\((.*?)\)[\s]*$/s', $properties, $matches);
        if (!isset($matches[1])) {
            return [];
        }
        $properties = $matches[1];

        preg_match_all(
            '/(?P<property>[\w]+)(\=("(?P<value1>.*?)"|\'(?P<value2>.*?)\'|(?P<value3>[^\s,]*))|)([\s]+|$|,)/msu',
            $properties,
            $matches,
            PREG_SET_ORDER
        );

        $return = array();
        foreach ($matches as $match) {
            $property = $match['property'];
            if (!empty($match['value1'])) { // Value encapsulated with double quotes (")
                $value = $match['value1'];
            } elseif (!empty($match['value2'])) { // Value encapsulated with single quotes (')
                $value = $match['value2'];
            } elseif (!empty($match['value3'])) { // Non-encapsulated value
                $value = $match['value3'];
            } else {
                $value = true;
            }
            $return[$property] = $value;
        }

        return $return;
    }
}
