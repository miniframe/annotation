<?php

namespace Miniframe\Annotation\Service;

use App\Annotation\BrokenAnnotation;
use App\Annotation\FoodBarAnnotation;
use App\Model\ModelWithAutoloadedAnnotation;
use App\Model\ModelWithBrokenAnnotation;
use App\Model\ModelWithoutDocblock;
use App\Model\ModelWithUnknownAnnotation;
use App\Model\TestModel;
use Miniframe\Annotation\Annotation\BaseAnnotation;
use Miniframe\Annotation\AutoloadedAnnotation;
use PHPUnit\Framework\TestCase;

class AnnotationReaderTest extends TestCase
{
    /**
     * Tests property annotations
     *
     * @return void
     */
    public function testPropertyAnnotations(): void
    {
        $class = (new AnnotationReader())->getClass(new \ReflectionClass(TestModel::class));
        $properties = $class->getProperties();

        // Static property should not be included
        $this->assertArrayNotHasKey('fooBar', $properties);

        // Foo property tested completely
        $fooProperty = $properties['foo'];
        $this->assertIsArray($fooProperty->getAnnotations());
        $this->assertEquals('foo', $fooProperty->getName());
        $this->assertEquals('private', $fooProperty->getAccessLevel());
        $this->assertInstanceOf(BaseAnnotation::class, $fooProperty->getAnnotation('var'));
        $this->assertEquals('var', $fooProperty->getAnnotation('var')->name);
        $this->assertEquals('string', $fooProperty->getAnnotation('var')->value);

        // Baz property is protected
        $bazProperty = $class->getProperty('baz');
        $this->assertEquals('protected', $bazProperty->getAccessLevel());

        // testFoo property is public
        $testFooProperty = $properties['testFoo'];
        $this->assertEquals('public', $testFooProperty->getAccessLevel());
    }

    /**
     * Tests class annotations
     *
     * @return void
     */
    public function testClassAnnotations(): void
    {
        $class = (new AnnotationReader())->getClass(new \ReflectionClass(TestModel::class));

        // Singular get
        $annotation = $class->getAnnotation(FoodBarAnnotation::class);
        $this->assertInstanceOf(FoodBarAnnotation::class, $annotation);
        $this->assertEquals('test', $annotation->foo);
        $this->assertEquals(1337, $annotation->bar);
        $this->assertEquals(true, $annotation->baz);
        $this->assertEquals(true, $annotation->bool);
        $this->assertEquals(12.48, $annotation->floating);

        // Plural get
        $annotations = $class->getAnnotations();
        $this->assertIsArray($annotations);
        $annotation = $annotations[FoodBarAnnotation::class];
        $this->assertInstanceOf(FoodBarAnnotation::class, $annotation);
        $this->assertEquals('test', $annotation->foo);
        $this->assertEquals(1337, $annotation->bar);
        $this->assertEquals(true, $annotation->baz);
        $this->assertEquals(true, $annotation->bool);
        $this->assertEquals(12.48, $annotation->floating);
    }

    /**
     * Tests class properties
     *
     * @return void
     */
    public function testClassProperties(): void
    {
        $class = (new AnnotationReader())->getClass(new \ReflectionClass(TestModel::class));

        $this->assertEquals('App\\Model\\TestModel', $class->getFullyQualifiedClassName());
        $this->assertEquals('TestModel', $class->getShortClassName());
    }

    /**
     * Tests that an unknown annotation becomes a base annotation
     *
     * @return void
     */
    public function testUnknownAnnotation(): void
    {
        $class = (new AnnotationReader())->getClass(new \ReflectionClass(ModelWithUnknownAnnotation::class));
        $annotations = $class->getAnnotations();
        $this->assertIsArray($annotations);
        $this->assertInstanceOf(BaseAnnotation::class, $annotations['nonexistentannotation']);
    }

    /**
     * Tests loading an annotation in the default namespace
     *
     * @return void
     */
    public function testAutoloadedAnnotation(): void
    {
        $class = (new AnnotationReader())->getClass(new \ReflectionClass(ModelWithAutoloadedAnnotation::class));
        $annotation = $class->getAnnotation(AutoloadedAnnotation::class);
        $this->assertInstanceOf(AutoloadedAnnotation::class, $annotation);
        $this->assertEquals('bar', $annotation->foo);
    }
    /**
     * Tests a class with no docblock
     *
     * @return void
     */
    public function testNoDocblock(): void
    {
        $class = (new AnnotationReader())->getClass(new \ReflectionClass(ModelWithoutDocblock::class));
        $this->assertIsArray($class->getAnnotations());
        $this->assertCount(0, $class->getAnnotations());
    }

    /**
     * Tests a class with a broken annotation
     *
     * @return void
     */
    public function testBrokenAnnotation(): void
    {
        $class = (new AnnotationReader())->getClass(new \ReflectionClass(ModelWithBrokenAnnotation::class));
        $this->assertInstanceOf(BaseAnnotation::class, $class->getAnnotation(BrokenAnnotation::class));
    }
}
