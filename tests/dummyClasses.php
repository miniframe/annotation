<?php

/**
 * Below are dummy classes with all sorts of annotations to test on
 */

namespace App\Model;

use App\Annotation\BrokenAnnotation;
use App\Annotation\FoodBarAnnotation;

/**
 * Test model class
 *
 * @FoodBarAnnotation(foo="test", bar=1337, bool, baz=true, floating=12.48)
 */
class TestModel
{
    /**
     * This variable is used to test reading the annotations. Beyond that, there's no use for this variable.
     *
     * @var string
     */
    private static $fooBar = 'BAR';

    /**
     * This variable is used to test reading the annotations. Beyond that, there's no use for this variable.
     *
     * @var string
     */
    private $foo = 'bar';

    /**
     * This variable is used to test reading the annotations. Beyond that, there's no use for this variable.
     *
     * @var string
     */
    protected $baz = '1337';

    /**
     * This variable is used to test reading the annotations. Beyond that, there's no use for this variable.
     *
     * @var string
     */
    public $testFoo = 'testBar';
}

/**
 * Class with an annotation that doesn't resolve
 *
 * @NonExistentAnnotation(foo=bar)
 */
class ModelWithUnknownAnnotation
{
}

/**
 * This annotation has no use statement but will be found by the default namespace
 *
 * @AutoloadedAnnotation(foo='bar')
 */
class ModelWithAutoloadedAnnotation
{
}

// This class doesn't have valid docblocks
class ModelWithoutDocblock
{
}

/**
 * Model with a broken annotation
 *
 * @BrokenAnnotation
 */
class ModelWithBrokenAnnotation
{
}

namespace App\Annotation;

/**
 * Test annotation
 *
 * @Annotation
 */
class FoodBarAnnotation
{
    /**
     * Textual annotation
     *
     * @var string
     */
    public $foo;

    /**
     * Numeral annotation
     *
     * @var int
     */
    public $bar;

    /**
     * Boolean annotation
     *
     * @var bool
     */
    public $baz;

    /**
     * Float annotation
     *
     * @var float
     */
    public $floating;

    /**
     * Another boolean
     *
     * @var bool
     */
    public $bool;
}

/**
 * A broken annotation
 *
 * @Annotation
 */
class BrokenAnnotation
{
    /**
     * Initializes the annotation
     */
    public function __construct()
    {
        throw new \RuntimeException('This annotation is broken');
    }
}

namespace Miniframe\Annotation;

/**
 * Autoloaded annotation
 *
 * @Annotation
 */
class AutoloadedAnnotation
{
    /**
     * Textual property
     *
     * @var string
     */
    public $foo;
}
