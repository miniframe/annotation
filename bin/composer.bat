@echo off
rem This is a Docker shortcut for running unit tests on this project

docker.exe run --rm --interactive --tty --volume "%~dp0\..:/var/www/html" garrcomm/php-apache-composer:7.3 composer %*
if "%ERRORLEVEL%" == "9009" (
    echo Docker for Windows is not installed.
    goto :eof
)
