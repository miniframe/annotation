@echo off
rem This is a Docker shortcut for running php static analyser on this project

docker.exe run --rm --interactive --tty --volume "%~dp0\..:/var/www/html" garrcomm/php-apache-composer:7.3 vendor/bin/phpstan %*
if "%ERRORLEVEL%" == "9009" (
    echo Docker for Windows is not installed.
    goto :eof
)
if "%ERRORLEVEL%" == "127" (
    echo Could not execute command. Have you ran "composer install" first?
    goto :eof
)
